package password;
/**?
 * 
 * @author khoav
 *
 */
public class password {
	private static int MIN_LENGTH = 8;
	public static boolean isValidLength( String password) {
		if (password.indexOf(" ") >= 0 ) {
			return false;
		}
		return  password.trim().length()>=MIN_LENGTH;
		
	}
	public static boolean isTwoDigit (String password) {
		int counter = 0;
		boolean found = false;
		for(int i = 0; i<password.length();i++) {
			char test = password.charAt(i);
			if(Character.isDigit(test))
				counter ++;
			if(counter == 2) {
				found = true;
				break;
			}
		}
		return found;
	}
	
}
