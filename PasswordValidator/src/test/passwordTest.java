package test;

import static org.junit.Assert.*;

import org.junit.Test;

import password.password;

public class passwordTest {
	@Test
	public void testPasswordValidatorLength() {
		boolean result = password.isValidLength("1234567890");
		assertTrue("invalid length",result);
	}
	@Test
	public void testPasswordValidatorLengthException() {
		boolean result = password.isValidLength("");
		assertFalse("invalid length",result);
	}
	@Test
	public void testPasswordValidatorLengthExceptionSpaces() {
		boolean result = password.isValidLength("    t e s t         ");
		assertFalse("invalid length",result);
	}
	@Test
	public void testPasswordValidatorLengthBoundaryIn() {
		boolean result = password.isValidLength("        ");
		assertFalse("invalid length",result);
	}
	@Test
	public void testPasswordValidatorLengthBoundaryOut() {
		boolean result = password.isValidLength("12345");
		assertFalse("invalid length",result);
	}
	@Test
	public void testPasswordTwoDigit() {
		boolean result = password.isTwoDigit("passwo12");
		assertTrue("invalid length",result);
	}
	@Test
	public void testPasswordTwoDigitException() {
		boolean result = password.isTwoDigit("passwo1d");
		assertFalse("invalid length",result);
	}
	@Test
	public void testPasswordTwoDigitBoundaryIn() {
		boolean result = password.isTwoDigit("passw12");
		assertTrue("invalid length",result);
	}
	@Test
	public void testPasswordTwoDigitBoundaryOut() {
		boolean result = password.isTwoDigit("password");
		assertFalse("invalid length",result);
	}
}
